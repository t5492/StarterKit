// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBVf18b4cpKDOqxTf4GqGWRO8qTO5-1SfQ",
    authDomain: "todolist-1d2c6.firebaseapp.com",
    projectId: "todolist-1d2c6",
    storageBucket: "todolist-1d2c6.appspot.com",
    messagingSenderId: "338697162941",
    appId: "1:338697162941:web:06590fd3dd59af5f6de7d4",
    measurementId: "G-Z3VJ7YBB6B"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
